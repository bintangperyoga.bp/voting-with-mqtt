import paho.mqtt.client as mqtt
from os import system, name
from getpass import getpass
import xmlrpc.client


def clrscr():
    if name == 'nt':
        _ = system('cls')
    else:
        _ = system('clear')


def vote_candidate():  # Main voting algorithm
    broker = "127.0.0.1"
    client = mqtt.Client("P2")
    client.connect(broker)
    client.loop_start()
    print("Please vote your choice:")
    print("1. Hanafi Abdullah Gusman")
    print("2. Bintang Peryoga")
    print()
    voting = 0
    while voting != "1" and voting != "2":
        voting = input("Input your choice (1 or 2): ")
        if voting != "1" and voting != "2":
            print("Please input a valid number! (1 or 2)")
            print()

    client.publish("vote", voting)
    client.disconnect()
    return 0


def splash_menu():
    print("Welcome to voting system!")
    print()
    print("1. Login")
    print("2. Register")
    print("0. Exit")
    print()
    user_choice = 0
    choices = ["0", "1", "2"]
    while user_choice not in choices:
        user_choice = input("Please input your choice: ")
        if user_choice not in choices:
            print("Please input a valid choice!")
            print()
    return user_choice


def login():
    s = xmlrpc.client.ServerProxy('http://127.0.0.1:8008')
    user_data = []
    userdata = s.user_db(0)
    userdata = list(userdata.data.decode("ascii").split("\r\n"))
    for i in range(len(userdata)-1):
        data = userdata[i].split(",")
        user_data.append([int(data[0]), data[1], data[2], int(data[3])])

    username = input("Username: ")
    username_exist = False
    for i in user_data:
        if i[1] == username:
            username_exist = True
            id_of_uname = i[0]
    if username_exist:
        password = getpass("Password: ")
        if user_data[int(id_of_uname)][2] == password:
            return 1, int(id_of_uname), user_data
        else:
            return "Wrong password!", 0, 0
    else:
        return "Username does not exist!", 0, 0


def register():
    s = xmlrpc.client.ServerProxy('http://127.0.0.1:8008')
    user_data = []
    userdata = s.user_db(0)
    userdata = list(userdata.data.decode("ascii").split("\r\n"))
    for i in range(len(userdata) - 1):
        data = userdata[i].split(",")
        user_data.append([int(data[0]), data[1], data[2], int(data[3])])

    print("Register your data")
    new_uname = input("Username: ")
    taken = False
    for i in user_data:
        if i[1] == new_uname:
            taken = True
    if taken:
        return 0
    else:
        new_pword = getpass("Password: ")
        user_data.append([len(user_data), new_uname, new_pword, 0])
        s.user_db(user_data)
        return 1


def user_menu():
    print("Welcome to voting system!")
    print()
    print("1. Vote now!")
    print("2. See candidate profile")
    print("3. Quick count")
    print("4. About KPU")
    print("0. Logout")
    print()
    user_choice = 0
    choices = ["0", "1", "2", "3", "4"]
    while user_choice not in choices:
        user_choice = input("Please input your choice: ")
        if user_choice not in choices:
            print("Please input a valid choice!")
            print()
    return user_choice


def quick_count():
    s = xmlrpc.client.ServerProxy('http://127.0.0.1:8008')
    data_kandidat = []
    votedata = s.vote_db()
    votedata = list(votedata.data.decode("ascii").split(","))
    for i in range(len(votedata) - 1):
        data_kandidat.append(int(votedata[0]))
        data_kandidat.append(int(votedata[1]))

    if data_kandidat[0] != 0 or data_kandidat[1] != 0:
        print("1. Hanafi Abdullah Gusman")
        print((data_kandidat[0] / (data_kandidat[0] + data_kandidat[1])) * 100, "%")
        print(data_kandidat[0], "pemilih")
        print()
        print("2. Bintang Peryoga")
        print((data_kandidat[1] / (data_kandidat[0] + data_kandidat[1])) * 100, "%")
        print(data_kandidat[1], "pemilih")
        print()
    else:
        print("Voting data not yet available.")


while True:
    clrscr()
    choice = splash_menu()
    if choice == "1":
        clrscr()
        login_status, cur_id, user_data = login()
        if login_status == 1:
            print("Login success!")
            acc = input("Press enter to continue")
            clrscr()
            while True:
                next_choice = user_menu()
                clrscr()
                if next_choice == "1":
                    if int(user_data[cur_id][3]) == 0:
                        run_vote = vote_candidate()
                        print()
                        leave = input("Vote succeeded. Press enter to continue...")
                        user_data[cur_id][3] = 1
                        s = xmlrpc.client.ServerProxy('http://127.0.0.1:8008')
                        s.user_db(user_data)
                        clrscr()
                    else:
                        leave = input("You have already voted!. Press enter to continue...")
                        clrscr()
                elif next_choice == "2":
                    s = xmlrpc.client.ServerProxy('http://127.0.0.1:8008')
                    userdata = s.profile().data.decode("utf-8")
                    print(userdata)
                    leave = input("Press enter to go back...")
                    clrscr()
                elif next_choice == "3":
                    quick_count()
                    leave = input("Press enter to go back...")
                    clrscr()
                elif next_choice == "4":
                    s = xmlrpc.client.ServerProxy('http://127.0.0.1:8008')
                    userdata = s.about().data.decode("utf-8")
                    print(userdata)
                    leave = input("Press enter to go back...")
                    clrscr()
                else:
                    clrscr()
                    break
        else:
            print(login_status)
            acc = input("Press enter to continue")
            clrscr()
    elif choice == "2":
        clrscr()
        reg_status = register()
        if reg_status == 1:
            print("Registration success!")
            acc = input("Press enter to continue")
            clrscr()
        else:
            print("Username already taken!")
            acc = input("Press enter to continue")
            clrscr()
    elif choice == "0":
        break
