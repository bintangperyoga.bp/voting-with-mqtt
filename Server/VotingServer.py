import paho.mqtt.client as mqtt
import csv
from xmlrpc.server import SimpleXMLRPCServer
import xmlrpc.client

# Ambil data jumlah vote dari file vote_data
data_kandidat = []
with open('vote_data.csv', mode='r') as votedata_raw:
    votedata = csv.reader(votedata_raw)
    for row in votedata:
        data_kandidat.append(int(row[0]))
        data_kandidat.append(int(row[1]))


with SimpleXMLRPCServer(("127.0.0.1", 8008)) as server:
    def user_db(type):
        if type == 0:
            with open("user_data.csv", 'rb') as handle:
                binary_data = xmlrpc.client.Binary(handle.read())
                return binary_data
        else:
            with open("user_data.csv", mode='w') as handle:
                index = csv.writer(handle, lineterminator='\n')
                for data in type:
                    index.writerow(data)
                return True
    
    def profile():
        with open("candidate_profile.txt", 'rb') as handle:
            binary_data = xmlrpc.client.Binary(handle.read())
            return binary_data

    def about():
        with open("about_kpu.txt", 'rb') as handle:
            binary_data = xmlrpc.client.Binary(handle.read())
            return binary_data
            
    def vote_db():
        with open("vote_data.csv", 'rb') as handle:
            binary_data = xmlrpc.client.Binary(handle.read())
            return binary_data
    
    server.register_function(user_db)
    server.register_function(profile)
    server.register_function(about)
    server.register_function(vote_db)
    server.register_introspection_functions()


    def on_connect(client, userdata, flags, rc):
        print("Connected")
        client.subscribe("vote")


    def on_message(client, userdata, message):
        if message.payload.decode() == "1":
            data_kandidat[0] += 1
        elif message.payload.decode() == "2":
            data_kandidat[1] += 1
        print("###########################")
        print("Hasil voting sementara:")
        print("Hanafi:", data_kandidat[0])
        print("Bintang:", data_kandidat[1])
        print("###########################")
        print()
        with open('vote_data.csv', mode="w") as csv_output:
            index = csv.writer(csv_output, lineterminator='\n')
            index.writerow(data_kandidat)


    broker = "127.0.0.1"
    print("Creating new instance...")
    client = mqtt.Client("P1")
    print("Connecting to broker...")
    client.connect(broker)

    client.on_connect = on_connect
    client.on_message = on_message

    client.loop_start()
    x = data_kandidat[0]
    y = data_kandidat[1]
    print("Hanafi:", x)
    print("Bintang:", y)

    server.serve_forever()