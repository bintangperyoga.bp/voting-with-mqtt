# Voting with MQTT

This is the final project of Distributed and Parallel System.
The topic is Voting Program with MQTT method.

Members:
* Bintang Peryoga(1301164032)
* M. Naufal Mukhbit A(1301162314)
* Riska Chairunnisa(1301164546)
* Shinta Surya S.U.(1301164410)

Voting with mqtt method implements the concept of publisher and subscriber, which
the user as the voter will act as the publisher and the server as the vote
collector will act as the subscriber. The server will subscribe to the same topic
as the user (publisher), so when the user published their choice of vote, the
server will got the message containing the vote data and will collect and process
it. Then the server will show the vote tally, both in percentage and actual
numbers. The tally will be constantly updated every time a new vote data came in.

Voting dengan mqtt menerapkan konsep subscriber dan publisher, di mana user
sebagai voter akan menjadi publisher dan server sebagai penerima vote akan
menjadi subscriber. Server akan subscribe ke topik yang sama dengan topik
yang dipilih user (publisher), sehingga ketika user mempublish pilihan voting
nya, server akan mendapatkan pesan yang berisi pilihan vote tersebut dan akan
merekap hasil vote tersebut. Server akan secara konstan menampilkan hasil rekapan
berupa persentase tiap kandidat juga jumlah pemilihnya, yang akan terus di-
update tiap terdapat votingan baru yang masuk.